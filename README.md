# Django Url Shortener

## Quick Start
1. Create .env file inside project main directory then create DB_USER and DB_PASSWORD variables. (Here's how: variable_name=value)
2. docker-compose up -d --build
3. Create django_url_shortener postgres database. Connect to container using command: docker exec -it *postgres_container_id* bash
4. Note: You must create database by user that specify on DB_USER variable. So create the user in postgres first and then create db using that user.

## How it works?
- /admin (GET)
- /api/v1/register (POST)
- /api/v1/login (POST)
- /api/v1/reset-password (POST)
- /api/v1/users (GET, PATCH, DELETE)
- /api/v1/urls (GET, POST, PATCH)
- /api/v1/urls/<key_of_short_url> (GET, DELETE)

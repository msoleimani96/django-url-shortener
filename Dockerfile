FROM python:3.8.14

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV DEVDIR="/code"

EXPOSE 8000

# Set work directory
WORKDIR ${DEVDIR}

# Install dependencies
COPY requirements.txt ${DEVDOR}
RUN pip install -r requirements.txt

# Copy project
COPY . ${DEVDIR}

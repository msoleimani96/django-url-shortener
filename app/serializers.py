from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password']
        extra_kwargs = {'email': {'required': True}}

    def save(self, validated_data):
        user = User(
            username=validated_data['email'], email=validated_data['email'])
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserPatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', ]
        extra_kwargs = {'email': {'required': True}}

    def save(self, current_user_id, validated_data):
        user = User.objects.get(id=current_user_id)
        user.email = validated_data['email']
        user.username = validated_data['email']
        user.save()
        return user


class ResetPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class UrlSortingSerializer(serializers.Serializer):
    choices = ['oldest', 'latest', 'views', 'views_desc']
    sort_as = serializers.ChoiceField(choices=choices)

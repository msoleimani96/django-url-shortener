import json
from math import perm
from os import stat
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
import uuid
from .models import *
from .serializers import *
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import *
import validators
from django.shortcuts import render
from django.core import serializers
from django.contrib.auth.hashers import check_password
from django.db.models import Count
from rest_framework.pagination import PageNumberPagination


@api_view(['GET', 'POST', 'PATCH'])
def app(request):
    request_method = request.method

    if request_method == 'GET':
        sort_as = request.GET.get('sort_as', None)
        if sort_as == None:
            sort_as = 'latest'
        serializer = UrlSortingSerializer(data={'sort_as': sort_as})

        if serializer.is_valid():
            sort_as = serializer.validated_data['sort_as']
            user = User.objects.get(
                username=request.user.email)
            if sort_as == 'oldest':
                urls = Url.objects.filter(user=user).values()
                return response_pagination(urls, request=request)
            elif sort_as == 'latest':
                urls = Url.objects.filter(user=user).order_by('-id').values()
                return response_pagination(urls, request=request)
            elif sort_as == 'views_desc':
                urls = Url.objects.filter(user=user).annotate(
                    num_views=Count('views')).order_by('num_views').values()
                return response_pagination(urls, request=request)
            else:
                # here sort is based on views
                urls = Url.objects.filter(user=user).annotate(num_views=Count('views')).order_by(
                    '-num_views').values()
                return response_pagination(urls, request=request)
        else:
            return response_message(serializer.errors, status_code=400)

    url = request.data['url']
    url_validator = validators.url(url)
    if url != None and len(url) != 0 and url_validator:
        if request_method == 'POST':
            key = generate_key()
            Url.objects.create(url=url, key=key, user=request.user)
            return response_data({'url': request.build_absolute_uri(f"/{key}")})
        else:
            # here the request method must be PATCH
            if 'key' in request.data:
                key = request.data['key']
                try:
                    url_obj = Url.objects.get(key=key)
                    url_obj.url = url
                    url_obj.save()
                except:
                    return response_message('Url not found.', 404)

                url_id = url_obj.id
                url_obj = Url.objects.get(id=url_id)
                return response_data(serializeModelObj(url_obj))
            else:
                return response_message(
                    'You have to provide the key of url to update it.', 400)

    else:
        error_msg = ''
        if url == None:
            error_msg = 'Url not provided in request body.'
        elif len(url) == 0:
            error_msg = 'Length of provided url is 0.'
        elif not url_validator:
            error_msg = 'Provided url is not valid.'
        return response_message(error_msg, 400)


@ api_view(['GET'])
@ permission_classes([AllowAny])
def handle_short_url(request, key):
    try:
        url = Url.objects.get(key=key)
    except:
        context = {'url': None}
        return render(request, 'index.html', context)

    context = {'url': url}
    View().create(request, url)
    return render(request, 'index.html', context)


@ api_view(['GET', 'DELETE'])
def get_del_url(request, key):
    try:
        url = Url.objects.get(key=key)
        request_method = request.method
        if request_method == 'GET':
            return response_data(serializeModelObj(url))
        else:
            # here the method must be DELETE
            url.delete()
            return response_message('Url deleted.', 200)
    except:
        return response_message('Url not found.', 404)


@ api_view(['GET', 'PATCH', 'DELETE'])
def user(request):
    request_method = request.method
    user = request.user
    if request_method == 'GET':
        return response_data(data=serializeModelObj(user))
    elif request_method == 'PATCH':
        serializer = UserPatchSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            validated_data = serializer.validated_data
            if user.email != validated_data['email'] and not User.objects.filter(email=validated_data['email']).exists():
                serializer.save(user.id, validated_data)
                return response_message('User updated.')
            else:
                return response_message(
                    'This email already exists.', status_code=400)
        else:
            return response_message(serializer.errors, status_code=400)
    else:
        # Here the method is DELETE
        user.delete()
        return response_message('User deleted.')


@ api_view(['POST'])
@ permission_classes([AllowAny])
def register(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        username = serializer.validated_data['email']
        if User.objects.filter(username=username).exists():
            return response_message('User already exists.', status_code=409)
        user = serializer.save(serializer.validated_data)
        token = Token.objects.create(user=user)
        return response_data(token.key, dataKey='token')
    else:
        return response_message(serializer.errors, 400)


@ api_view(['POST'])
def reset_password(request):
    serializer = ResetPasswordSerializer(data=request.data)
    if serializer.is_valid():
        validated_data = serializer.validated_data
        user = request.user
        old_password = validated_data['old_password']
        new_password = validated_data['new_password']
        if check_password(old_password, user.password):
            if old_password == new_password:
                return response_message('Old password and new password are same.', status_code=400)
            if len(new_password) < 8:
                return response_message('Password length must be greater than 8.', status_code=400)
            user_id = user.id
            user = User.objects.get(id=user_id)
            user.set_password(validated_data['new_password'])
            user.save()
            return response_message('Password changed successfully.')
        else:
            return response_message('Invalid old password.', status_code=401)
    else:
        return response_message(serializer.errors, status_code=400)


def generate_key(length=10):
    key = str(uuid.uuid4())
    key = key.replace('-', '')
    return key[0:length - 1]


def response_message(msg, status_code=200):
    return Response({'msg': msg}, status=status_code)


def response_pagination(data, request,):
    # pagination config
    paginator = PageNumberPagination()
    paginator.page_size = 20
    result = paginator.paginate_queryset(data, request)
    return paginator.get_paginated_response(result)


def response_data(data, data_key='data'):
    return Response({data_key: data})


def serializeModelObj(obj):
    json_obj = serializers.serialize('json', [obj])
    return json.loads(json_obj)

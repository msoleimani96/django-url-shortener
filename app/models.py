from django.db import models
from django.contrib.auth.models import User
from .utilities import get_ip_location


class Url(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.CharField(max_length=500)
    key = models.CharField(max_length=10, unique=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='urls')

    def __str__(self):
        return self.url


class View(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.ForeignKey(
        Url, on_delete=models.CASCADE, related_name='views')
    location = models.CharField(max_length=150)

    def create(self, request, url):
        ip_location = get_ip_location()
        country = ip_location['country']
        region = ip_location['region']
        city = ip_location['city']
        view = View(url=url, location=f'{country}, {region}, {city}')
        view.save()
        return view

    def __str__(self):
        return self.url.url

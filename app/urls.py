from django.urls import path
from .views import *
from rest_framework.authtoken import views


urlpatterns = [
    path('urls/', view=app),
    path('urls/<str:key>/', view=get_del_url),

    path('users/', view=user),

    path('register/', view=register,),
    path('login/', view=views.obtain_auth_token),
    path('reset-password/', view=reset_password),
]
